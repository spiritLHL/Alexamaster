# alexamaster-auto Ubuntu/windows 

#### 理论上所有系统都适合，其他系统自行测试，大致套路是自己安装VNC和火狐浏览器挂机，我这个是一键脚本

注册网址：[点我](https://www.alexamaster.net/sec/join.php?id=163416)

(直连即可，一切操作不需要魔法)

注册邮箱选择163邮箱等，千万不要用QQ邮箱，QQ邮箱已经被禁。

注册后有需要你验证邮箱地址，需要```Withdraw Money```中的```REQUEST```页面点击第二个打了红色×的帖子，去绑定邮箱和寄送地址，才能满足提现条件。

(或者开启浏览器自动翻译，找到提现页面自行解决提现未满足的条件)

提现方式是paypal，国内卡直接注册即可，相关操作自行搜索。(CSDN或者知乎都有教程)

下面是具体操作

# 前提：Windows系统

得不偿失，有更好的挂机项目别挂这个，吃内存资源。

只需要下载火狐浏览器，将类似我的

https://www.alexamaster.net/ads/autosurf/163416

这样子的链接粘贴到地址栏访问即可挂机。

# 前提：Ubuntu系统

### step1：初始化

终端窗口逐行输入

国内机子：
```bash
wget https://gitee.com/spiritlhl/Alexamaster-onkey-start/attach_files/757494/download/vncam.sh

bash vncam.sh -p 'https://gitee.com/spiritlhl/Alexamaster-onkey-start/attach_files/757322/download/passwd-d10086' -u '你的auto surf链接'
```
国外机子：
```bash
wget https://github.com/spiritLHL/Alexamaster/releases/download/%E8%84%9A%E6%9C%AC/vncam.sh 

bash vncam.sh -p 'https://github.com/spiritLHL/Alexamaster/releases/download/%E8%84%9A%E6%9C%AC/passwd-d10086' -u '你的auto surf链接'
```

某些境外机子(甲骨文)有点特殊，用这一串命令：

```
sudo -i
sudo kill -9 $(pidof Xtightvnc)
sudo rm -rf /tmp/.X1-lock
sudo rm -rf /tmp/.X11-unix/X1
wget xiaofd.github.io/vncam.sh 
bash vncam.sh -p 'xiaofd.github.io/others/passwd-d10086' -u '你的auto surf链接'
```

这时候看看alexa官网有没有运行，运行了下面的```step2```也不用执行了。
#### 说明：

 注意-p和-u参数都是必要的

-p参数后面指定的是vnc登陆密码下载链接：使用我默认的这个密码为：d10086

-u参数后面放你自己的挂机链接

我的

https://www.alexamaster.net/ads/autosurf/163416

是我的挂机链接，有需要请自行挂网站里自己的auto surf链接

#### 非必须：

当安装完毕后可以通过命令 vncpasswd 直接修改vnc登陆密码（最长8位）

可以自己导出自己的密码文件，替换上面的-p参数，文件为 ~/.vnc/passwd

已知：

完美适用于腾讯云境外机子2核4g内存的Ubuntu20版本，实测没有任何问题

运行完脚本后即可开始挂机，已经安装火狐插件和关闭屏蔽弹窗，无需进入vnc配置 vnc登陆端口5901，默认vnc登陆密码d10086

### step2：启动挂机(非必须)

启动vnc桌面环境(逐行输入)
```bash

tightvncserver :1

export DISPLAY=localhost:1

firefox --profile ~/.alexa/alexa --new-tab 'https://www.alexamaster.net/ads/autosurf/163416' &

```
说明：

这里的

https://www.alexamaster.net/ads/autosurf/163416

是我的挂机链接，有需要请自行挂网站里自己的auto surf链接

### step3：设置定时重启避免卡住(非必须，实测基本不卡，卡的请再设置)

前提：宝塔面板

在宝塔中打开计划任务

设置shell脚本

#### 第一个：

任务名称：定时关闭VNC和火狐浏览器

执行周期：N小时 6 时 0 分

脚本内容
```
export DISPLAY=localhost:1
pkill firefox
```
#### 第二个：

任务名称：定时启动桌面和火狐浏览器

执行周期：N小时 6 时 5 分

脚本内容
```
export DISPLAY=localhost:1
firefox --profile ~/.alexa/alexa --new-tab 'https://www.alexamaster.net/ads/autosurf/163416' &
```
上面中启动的是我的链接，自己的自行替换。

操作后可能会有三行提示，不用管，那是因为脚本使用的是旧版火狐浏览器，提示更新，忽略即可。

实际测试占用还行，大概800M内存，18%的2核英特尔i5CPU和7.5G硬盘空间。

如果不是2核4G的云服务器，建议把周期从6小时改得更短。(我的不设置重启了，没卡过)

最低配置要求：1核1G内存10G硬盘

收入大概一天好的0.2美元，坏的0.1美元。

# 注意

~运行几天后有可能因为进程堵塞导致无法正常使用(查看官网收益可以看见自己的ip未运行时)，此时建议关闭VNC后重新启动桌面运行脚本。~

如果几天后突然卡死，终端执行：(实测2核4g内存的运行5天没卡过)

ps:下面那个163416得换成你的id，把http那个链接换成你的，不然执行是挂的我的链接。。。

```bash
sudo su ubuntu
sudo kill -9 $(pidof Xtightvnc)
sudo rm -rf /tmp/.X1-lock
sudo rm -rf /tmp/.X11-unix/X1
tightvncserver :1
export DISPLAY=localhost:1
firefox --profile ~/.alexa/alexa --new-tab 'https://www.alexamaster.net/ads/autosurf/163416' &
```

如果上面执行后官网还是IP显示dead，再执行下面操作。

关闭操作：

查看VNC进程

```bash
ps -ef|grep -i vnc
```

关闭那些VNC进程：

```bash
sudo kill -9 这里填显示的第二列PID值(一串数字)
```

删除运行文件：

```bash
sudo rm -rf /tmp/.X1-lock
sudo rm -rf /tmp/.X11-unix/X1
```

关闭进程时可能会在最后一个进程无法kill掉，建议：

```bash
sudo -i
sudo kill -9 这里填显示的第三列PID值(一串数字)
```

然后回到原来的用户：

```bash
sudo su ubuntu
```

上述脚本都执行后仍无法正常刷，建议重装脚本。(事先关闭VNC进程和删除运行文件后即可重装)

pps：如果还是不行，你看看是不是你的ipdead了，quality变成0%了，这个时候点击右边红色按钮重置IP才能继续挂，不然怎么脚本操作都没用的。

ppps：掉线太久就会出现pps里的情况，机子性能不行建议设置定时嗷，定时step3还是step2都行，我定时的step3里的那个一键脚本，记得换自己的autosurf地址!

# 提现注意事项：

不要攒到一起提现，每到1美元就进行提现，提现到账周期大概为1周，一次最多申请2~3个，申请额度高或次数过多，到账周期将以月计算。

个人实测2台机子24小时跑6天多一点点就够提现1美元到paypal了

paypal到账后不用立即提到卡上，手续费极其高昂，个人正在摸索好的便宜的提现方式，等我好消息。
